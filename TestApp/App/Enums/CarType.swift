//
//  CarType.swift
//  FastlaneTests
//
//  Created by Christoph G. on 16.03.18.
//  Copyright © 2018 APPSfactory GmbH. All rights reserved.
//

enum CarType {
	case economy
	case offRoad
	case sport
}
