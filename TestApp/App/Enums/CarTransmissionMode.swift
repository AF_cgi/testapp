//
//  CarTransmissionMode.swift
//  FastlaneTests
//
//  Created by Christoph G. on 16.03.18.
//  Copyright © 2018 APPSfactory GmbH. All rights reserved.
//

enum CarTransmissionMode {
	case park
	case reverse
	case neutral
	case drive
}
