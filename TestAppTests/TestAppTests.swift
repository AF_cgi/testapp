//
//  Created by APPSfactory GmbH
//  Copyright © 2017 APPSfactory GmbH. All rights reserved.
//

import XCTest
@testable import TestApp

class TestAppTests: XCTestCase {

	var ferrari: Car!
	var jeep: Car!
	var honda: Car!
	
	
	// MARK: - Life cycle
	
	override func setUp() {
		super.setUp()
		
		self.ferrari = Car(type: .sport, transmissionMode: .drive)
		self.jeep    = Car(type: .offRoad, transmissionMode: .drive)
		self.honda   = Car(type: .economy, transmissionMode: .park)
	}
	
	override func tearDown() {
		super.tearDown()
		
		self.ferrari = nil
		self.jeep    = nil
		self.honda   = nil
	}
	
	
	// MARK: - Test cases
	
	func testSportFasterThanJeep() {
		
		let minutes = 60
		self.ferrari.start(minutes: minutes)
		self.jeep.start(minutes: minutes)
		
		XCTAssertTrue(self.ferrari.miles > self.jeep.miles)
	}
}
