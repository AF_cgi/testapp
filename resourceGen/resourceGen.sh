#!/bin/bash

# insert the correct file id
fileId=""

if [ "$fileId" != "" ]
then

    sheet="sheet.xlsx"
    curl -f -o "$sheet" "https://docs.google.com/spreadsheets/d/$fileId/export?format=xlsx&id=$fileId"

    # Localizable strings
    java -jar StringGenerator-1.1.10.jar \
        -namedplaceholder placeholder.txt \
        -out ../TestApp/Resources/ \
        -os ios \
        -input "$sheet" \
        -table "Localizations" \
        -keys B \
        -lang 3 \
        -from C \
        -to C
    
    # Permission strings
    java -jar StringGenerator-1.1.10.jar \
        -namedplaceholder placeholder.txt \
        -out ../TestApp/Resources/ \
        -os ios \
        -input "$sheet" \
        -table "Permissions" \
        -keys C \
        -lang 3 \
        -from D \
        -to D \
        -fileName InfoPlist.strings

    # Tracking
    java -jar StringGenerator-1.1.10.jar \
        -namedplaceholder placeholder.txt \
        -out ../TestApp/Resources/ \
        -os ios \
        -input "$sheet" \
        -table "Tracking" \
        -keys B \
        -lang 3 \
        -from C \
        -to C \
        -fileName Tracking.strings

    # NonLocalizations
    java -jar StringGenerator-1.1.10.jar \
        -namedplaceholder placeholder.txt \
        -out ../TestApp/Resources/ \
        -os ios \
        -input "$sheet" \
        -table "NonLocalizations" \
        -keys B \
        -lang 3 \
        -from C \
        -to C \
        -fileName NonLocalizable.strings
    
    # Colors
    java -jar StringGenerator-1.1.10.jar \
        -out ../TestApp/Resources/ \
        -os android \
        -input "$sheet" \
        -table "Colors" \
        -keys B \
        -lang 2 \
        -from F \
        -to F \
        -fileName colors.xml \
        -resourceType "color"
    
    # copy colors.xml to correct directory
    cp ../TestApp/Resources/values/colors.xml ../TestApp/Resources/colors.xml
    rm -r ../TestApp/Resources/values

    # remove the temporary sheet file
    rm -r "$sheet"

fi
