# AFLogger

## Requirements Version >= 1.0.0
- Xcode 8
- Swift 3.0 
- iOS 8

## Requirements Older versions < 1.0.0
- Xcode 7
- iOS 8
- Swift < 2.3


##Features

###iOS
* activate/deactivate print output
* log-level output
* file logging
* onscreen live debugging in TextView
* open current and previous session log-files
* send log-files via E-Mail
* reset app content
* HockeyApp log attachment

###tvOS 
* activate/deactivate print output
* log-level output

## Installation

AFLogger is available through [CocoaPods](http://cocoapods.org). To access the private AF pod-spec-repo add the AF Bitbucket specs-repo-url to your Podfile.
```ruby
source 'https://bitbucket.org/appsfactoryde/af_iospodspecs'
```

To install it, simply add the following line to your Podfile:

```ruby
pod "AFLogger"
```

## Usage

Import AFLogger module in your Appdelegate.
```swift
import AFLogger
```

Create a reference to the AFLogger singleton outside of your Appdelegate class implementation.

```swift
let LOG = AFLogger.shared

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
...
```

__Log output is deactivated by default.__ To start the logging output call one of the designated activate-functions.

```swift
// iOS
LOG.activate(withOnscreenOutput: true)

// tvOS
LOG.activate()
```

Specify different log settings for each of your configurations. See example below.


```swift
#if CONFIGURATION_Debug
LOG.activate(withOnscreenOutput: true)
LOG.logLevel = .verbose

#elseif CONFIGURATION_Enterprise
LOG.activate(withOnscreenOutput: true)
LOG.logLevel = .info
LOG.disableOnscreenLogfiles = true
LOG.mailRecipients = ["xyz@appsfactory.de"]

#elseif CONFIGURATION_AppStore
LOG.activate(withOnscreenOutput: false)
LOG.logLevel = .info
#endif
```

To print logs via AFLogger in your app simply __use one of these log functions instead of "print" or "NSLog"__.
```swift
LOG.V("Verbose Log")  // not so important
LOG.D("Debug Log")	// something to debug  
LOG.I("Info Log")	 // nice to know
LOG.W("Warning Log")  // that's not good
LOG.E("Error Log")	// that's really bad
```

##Advanced Usage

###Log Level
Available log-levels
```swift
Verbose
Debug
Info
Warning
Error
```

Change the level via the logLevel property. The logs only will be printed for levels >= the specified level. In the given example all logs for the levels *Info*, *Warning* and *Error* will be printed.
```swift
LOG.logLevel = .info
```

Separated from the log-levels every call of viewWillAppear: will be logged to track the users screen flow. This could be disabled with:
```swift
LOG.isUiLoggingEnabled = false
```



###Mail 

Only enable the "send-mail" functionality in the onscreen popup. All other options, e.g. show log-files, will be hidden.
```swift
LOG.sendMailOnly = true
```

Preconfigure the mail recipients.
```swift
LOG.mailRecipients = ["xyz@appsfactory.de"]
```

###Misc 

Disable the ability to open the log files onscreen in TextView.
```swift
LOG.disableOnscreenLogfiles = true
```

Change the duration of the longpress gesture-recognizer.
```swift
LOG.longPressDuration = 3.0
```

Add custom actions to the UIAlertController used in the AFLogger.
```swift
LOG.addAlertAction(UIAlertAction(title: "Custom Action Title", style: .default, handler: { (_) -> Void in
	// do something
}))
```

Add additional functionality to the "reset" action.
```swift
LOG.additionalResetActions { () -> (Void) in
	//Add some more reset functionality
}
```

## HockeyApp

With the AFLogger it's possible to attach some logged data from the previously crashed session to the HockeyApp crash upload.

Add HockeySDK code as usual and __don't forget to set the delegate__

```swift
BITHockeyManager.shared().configure(withIdentifier: "XYZ", delegate: self)
BITHockeyManager.shared().start()
BITHockeyManager.shared().authenticator.authenticateInstallation()
```

Implement the Hockey Delegate and upload additional crash data provided by the AFLogger.

```swift
// MARK: - Hockey Delegate
extension AppDelegate: BITHockeyManagerDelegate
{
    func applicationLog(for crashManager: BITCrashManager!) -> String!
    {
        return LOG.getCrashLog(withBytes: 5000)
    }
}
```

## Author

cam, cam@appsfactory.de

## License

AFLogger is available under the MIT license. See the LICENSE file for more info.
